# tech-solution

## How to Use (IMPORTANT)

This project has a fake backend based on the json-server library, you must have installed node, express (and yarn would be optimal).

To use json-server you must run

```
# NPM 
npm install -g json-server json-server-auth
 
# Yarn 
yarn add -g json-server json-server-auth

```
and run 

```
json-server-auth db.json

```
to start the fake server.

Please note that the database information fetching for the user works only with the first registered user. (To delete you must clear the users array in db.json)

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Suggested links

[Json Server](https://www.npmjs.com/package/json-server)
[Json Server Auth](https://www.npmjs.com/package/json-server-auth)